Theme: Cleartabs
Creator: PAULTHEMES, www.paulthemes.com
Published: 21.04.2011
Branch: Drupal 7

SPECIFICATION:

Maximum size of logo in pixels (width x height):
260 x 70

Available regions:

Header
Help
Highlighted
Featured
Content
Sidebar first
Sidebar second
Before footer one
Before footer two
Before footer three
Footer one
Footer two
Footer three
Footer four
Footer

