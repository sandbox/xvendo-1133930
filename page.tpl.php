<div class="page" >
  <div class="header">
    <div class="layout-wrapper">
      <div class="header-regions">
        <div id="top-region-left" class="dtw-clearfix">
          <div class="logo">
          <?php if ($logo): ?>
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
              <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
            </a>
          <?php endif; ?>
          </div><!-- logo end -->
          <div class="slogan">
          <?php if ($site_slogan): ?>
	    <?php print $site_slogan; ?>
          <?php endif; ?>
          </div><!-- slogan end -->
        </div><!-- top region left end -->
        <div id="top-region-right" class="dtw-clearfix">
          <div class="secondary">
          <?php if ($secondary_menu): ?>
              <?php print theme('links__system_secondary_menu', array(
                'links' => $secondary_menu,
                'attributes' => array(
                'id' => 'secondary-menu-links',
                'class' => array('links', 'inline', 'clearfix'),
                ),
                  'heading' => array(
                  'text' => t('Secondary menu'),
                  'level' => 'h2',
                  'class' => array('element-invisible'),
                  ),
             )); ?>
           <?php endif; ?>
          </div><!-- secondary links end -->
          <div class="header-region">
            <?php print render($page['header']); ?>
          </div>
        </div><!-- top region right end -->
      </div>        
    </div>
  </div><!-- header end -->
  <div class="navigation">
    <div class="layout-wrapper">
    <?php if ($main_menu): ?>
      <div id="main-menu">
        <?php print theme('links__system_main_menu', array(
          'links' => $main_menu,
          'attributes' => array(
            'id' => 'main-menu-links',
            'class' => array('links', 'clearfix'),
          ),
          'heading' => array(
            'text' => t('Main menu'),
            'level' => 'h2',
            'class' => array('element-invisible'),
          ),
        )); ?>
      </div> <!-- /#main-menu -->
    <?php endif; ?>
    </div>
  </div><!-- navigation end -->

  <?php if ($title): ?>
  <div class="sitetitle-layout">
    <div class="layout-wrapper">
      <h1 class="title" id="page-title"><?php print $title; ?></h1>
    </div>
  </div>
  <?php endif; ?>

  <?php if ($page['featured']): ?>
  <div id="featured-layout" class="dtw-clearfix">
    <div class="layout-wrapper">
      <?php print render($page['featured']); ?>
    </div>
  </div><!-- featured layout end -->
  <?php endif; ?>

  <div id="threecol-layout" class="dtw-clearfix">
    <div class="layout-wrapper">
      <div class="twocols"> 
        <div class="maincol">
        <?php if ($page['highlighted']): ?>
        <div id="highlighted"><?php print render($page['highlighted']); ?></div>
        <?php endif; ?>
        <a id="main-content"></a>
        <?php print render($title_prefix); ?>
        <?php print render($title_suffix); ?>
        <?php if ($tabs): ?>
        <div class="tabs">
          <?php print render($tabs); ?>
        </div>
        <?php endif; ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
        <?php endif; ?>
        <?php print render($page['content']);?>
        <?php print $feed_icons; ?>
        </div><!-- content end -->
        <?php if ($page['sidebar_second']): ?>
	<div class="rightcol"><?php print render($page['sidebar_second']);?></div>
        <?php endif; ?><!-- sidebar second end -->
      </div>
      <?php if ($page['sidebar_first']): ?>
      <div class="leftcol"><?php print render($page['sidebar_first']);?></div>
      <?php endif; ?><!-- sidebar first end -->
    </div>
  </div><!-- threecol layout end -->

  <?php if ($page['before_footer_one'] || $page['before_footer_two'] ||
            $page['before_footer_three']): ?>
  <div id="before-footer-layout" class="dtw-clearfix">
    <div class="layout-wrapper">
      <div class="col-one"><?php print render($page['before_footer_one']);?></div>
      <div class="col-two"><?php print render($page['before_footer_two']);?></div>
      <div class="col-three"><?php print render($page['before_footer_three']);?></div>
    </div>
  </div><!-- before footer layout end -->
  <?php endif; ?>

  <?php if ($page['footer_one'] || $page['footer_two'] ||
            $page['footer_three'] || $page['footer_four'] ): ?>
  <div id="footer-layout" class="dtw-clearfix">
    <div class="layout-wrapper">
      <div class="col-one"><?php print render($page['footer_one']);?></div>
      <div class="col-two"><?php print render($page['footer_two']);?></div>
      <div class="col-three"><?php print render($page['footer_three']);?></div>
      <div class="col-four"><?php print render($page['footer_four']);?></div>
    </div>
  </div><!-- footer layout end -->
  <?php endif; ?>

  <?php if ($page['footer']): ?>
  <div id="after-footer-layout" class="dtw-clearfix">
    <div class="layout-wrapper">
      <?php print render($page['footer']);?>
    </div>
  </div><!-- footer layout end -->
  <?php endif; ?>

</div><!-- page end -->
</body>
</html>
