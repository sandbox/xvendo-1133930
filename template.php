<?php

/**
 * Add body classes if certain regions have content.
 */
function cleartabs_preprocess_html(&$variables) {
  if (!empty($variables['page']['featured'])) {
    $variables['classes_array'][] = 'featured';
  }

  if (!empty($variables['page']['before_footer_one'])
    || !empty($variables['page']['before_footer_two'])
    || !empty($variables['page']['before_footer_three'])) {
    $variables['classes_array'][] = 'before-footer-columns';
  }

  if (!empty($variables['page']['footer_one'])
    || !empty($variables['page']['footer_two'])
    || !empty($variables['page']['footer_three'])
    || !empty($variables['page']['footer_four'])) {
    $variables['classes_array'][] = 'footer-columns';
  }

  // Add conditional stylesheets for IE
  drupal_add_css(path_to_theme() . '/css/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'preprocess' => FALSE));
  drupal_add_css(path_to_theme() . '/css/ie6.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 6', '!IE' => FALSE), 'preprocess' => FALSE));

}

function cleartabs_menu_link(array $variables) {

  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  if ($sub_menu) {
    $element['#attributes']['class'][] = 'dir';
  }

  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Override or insert variables into the node template.
 */
function cleartabs_preprocess_node(&$variables) {
  $variables['submitted'] = t('published by !username on !datetime', array('!username' => $variables['name'], '!datetime' => $variables['date']));
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }
}


